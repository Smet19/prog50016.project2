#include "SettingsManager.h"
#include <iostream>
#include <fstream>

SettingsManager::SettingsManager()
{
	windowName = "Default";
	width = 640;
	height = 480;
	frameLimit = 60;
	fullscreen = false;
}

SettingsManager::~SettingsManager()
{
}

void SettingsManager::initialize()
{
	if (!initialized)
	{
		std::ifstream inputStream("./Settings/GameSettings.json");
		std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
		settings = json::JSON::Load(str);


		if (settings.hasKey("windowName"))
		{
			windowName = settings["windowName"].ToString();
		}

		if (settings.hasKey("width"))
		{
			width = settings["width"].ToInt();
		}

		if (settings.hasKey("height"))
		{
			height = settings["height"].ToInt();
		}

		if (settings.hasKey("frameLimit"))
		{
			frameLimit = settings["frameLimit"].ToInt();
		}

		if (settings.hasKey("uiFontPath"))
		{
			uiFontPath = settings["uiFontPath"].ToString();
		}


		if (settings.hasKey("fullscreen"))
		{
			fullscreen = settings["fullscreen"].ToBool();
		}

		if (settings.hasKey("map"))
		{
			std::ifstream inputStream(settings["map"].ToString());
			std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
			map = json::JSON::Load(str);
		}

		if (settings.hasKey("game_objects"))
		{
			std::ifstream inputStream(settings["game_objects"].ToString());
			std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
			game_objects = json::JSON::Load(str);
		}

		initialized = true;
	}
}
