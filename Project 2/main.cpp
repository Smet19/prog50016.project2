#include <SFML/Graphics.hpp>
#include "SettingsManager.h"
#include "GameObjectManager.h"
#include "GameObject.h"
#include "Tile.h"

int main()
{
	SettingsManager::Instance().initialize();

	sf::RenderWindow* window = new sf::RenderWindow(sf::VideoMode(
		SettingsManager::Instance().Width(),
		SettingsManager::Instance().Height()),
		SettingsManager::Instance().WindowName(),
		SettingsManager::Instance().Fullscreen()
		? sf::Style::Titlebar | sf::Style::Close | sf::Style::Fullscreen
		: sf::Style::Titlebar | sf::Style::Close);

	window->setFramerateLimit(SettingsManager::Instance().FrameLimit());

	GameObjectManager::Instance().load(SettingsManager::Instance().getGameObjects());

	json::JSON& map = SettingsManager::Instance().getMap();
	std::list<Tile*> tileMap;

	for (auto tile : map.ArrayRange())
	{
		Tile* gameObject = new Tile();
		gameObject->load(tile);
		tileMap.push_back(gameObject);
	}

	sf::Event event;
	while (window != nullptr)
	{
		window->clear();

		while (window != nullptr && window->pollEvent(event))
		{

			if (event.type == sf::Event::LostFocus)
			{
				break;
			}

			switch (event.type)
			{
			case sf::Event::Closed:
				window->close();
				delete window;
				window = nullptr;
				break;

			case sf::Event::KeyPressed:
				switch (event.key.code)
				{
				// Escape - Exit the game
				case sf::Keyboard::Escape:
					window->close();
					delete window;
					window = nullptr;
					break;

				}
				break;
			}
		}



		if (window != nullptr)
		{
			// We draw map first
			for (auto* tile : tileMap)
			{
				window->draw(*tile->getSprite());
			}

			// Then all the other objects
			for (auto object : GameObjectManager::Instance().objects)
			{
				window->draw(*object->getSprite());
			}

			window->display();
		}
	}

	for (auto* tile : tileMap)
	{
		delete tile;
	}
}
