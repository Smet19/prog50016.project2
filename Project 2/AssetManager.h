#pragma once
#ifndef _ASSETMANAGER_H_
#define	_ASSETMANAGER_H_

#include <string>
#include <map>
#include <SFML/Graphics.hpp>
#include "IInitializable.h"
#include "StandardTypes.h"

// Handles and stores all textures that are loaded while the game runs
class AssetManager :  public IInitializable
{
public:
	// Constructors / Destructors
	AssetManager();
	virtual ~AssetManager();

	// Methods
	void initialize() override;
	sf::Texture* getTexture(std::string _fileName); // Get texture by file path.

private:
	// Stores textures
	std::map<std::string, sf::Texture*> textureMap;

	DECLARE_SINGLETON(AssetManager)
};

#endif // !_ASSETMANAGER_H_

