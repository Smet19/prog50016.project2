#include "Character.h"

Character::Character()
{
	maxHealth = 100;
	health = maxHealth;
	dead = false;
}

Character::~Character()
{
}

void Character::Die()
{
	dead = true;
	health = 0;
}

void Character::Turn()
{
}

void Character::update()
{
	if (!dead)
	{
		if (health <= 0)
		{
			Die();
		}
		else
		{
			Turn();
		}
	}
}

void Character::load(json::JSON& _json)
{
	GameObject::load(_json);

	if (_json.hasKey("maxHealth"))
	{
		maxHealth = _json["maxHealth"].ToInt();
	}

	if (_json.hasKey("health"))
	{
		health = _json["health"].ToInt();
	}

	if (_json.hasKey("dead"))
	{
		dead = _json["dead"].ToBool();
	}
}
