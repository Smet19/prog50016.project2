#pragma once
#ifndef _TILE_H_
#define _TILE_H_

#include "Object.h"

class Tile : public Object
{
public:
	// Constructors / Destructors
	Tile() {};
	virtual ~Tile() {};

	virtual void update() override {};
};
#endif // !_TILE_H_