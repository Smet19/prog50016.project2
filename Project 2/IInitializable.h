#pragma once
#ifndef _INITIALIZABLE_H_
#define _INITIALIZABLE_H_

class IInitializable
{
public:
	virtual void initialize() = 0;
	bool isInitialized() { return initialized; }

protected:
	bool initialized;
};
#endif // !_INITIALIZABLE_H_