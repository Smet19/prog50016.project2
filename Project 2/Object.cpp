#include "Object.h"
#include "StandardTypes.h"

Object::Object()
{
	blockMovement = false;
	SetPosition(0, 0);
	texturePath = "./Resources/default.png";
}

void Object::load(json::JSON& _json)
{
	if (_json.hasKey("position"))
	{
		json::JSON vector = _json["position"];

		unsigned int x, y = 0;

		if (vector.hasKey("x"))
			x = vector["x"].ToInt();

		if (vector.hasKey("y"))
			y = vector["y"].ToInt();

		SetPosition(x, y);
	}

	if (_json.hasKey("blockMovement"))
		blockMovement = _json["blockMovement"].ToBool();

	if (_json.hasKey("texturePath"))
	{
		texturePath = _json["texturePath"].ToString();
		setTexture(AssetManager::getInstance().getTexture(texturePath));
	}
}

void Object::SetPosition(const sf::Vector2i& _pos)
{
	position = _pos;
	sprite.setPosition(sf::Vector2f(position * TILESIZE));
}

void Object::SetPosition(unsigned int x, unsigned int y)
{
	position.x = x;
	position.y = y;
	sprite.setPosition(x * TILESIZE, y * TILESIZE);
}

void Object::Move(const sf::Vector2i& _pos)
{
	position += _pos;
	sprite.move(sf::Vector2f(position * TILESIZE));
}

void Object::Move(unsigned int x, unsigned int y)
{
	position.x += x;
	position.y += y;
	sprite.move(x * TILESIZE, y * TILESIZE);
}

bool Object::IsColliding(Object* _target)
{
	return _target->position == position;
}

void Object::OnCollision(Object* _target)
{
}