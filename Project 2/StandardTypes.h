#ifndef _STANDARDTYPES_H_
#define _STANDARDTYPES_H_
#pragma once

#define TILESIZE 256

#define DECLARE_SINGLETON(className)\
public:\
	inline static className& Instance() \
	{\
		static className instance;\
		return instance;\
	}\
private:\
	className();\
	~className();\
	inline explicit className(className const&) = delete;\
	inline className& operator=(className const&) = delete;



#endif