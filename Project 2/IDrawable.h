#pragma once
#ifndef _DRAWABLE_H_
#define _DRAWABLE_H_

#include "IInitializable.h"
#include <SFML/Graphics.hpp>


// Interface that handles work with sprites
class IDrawable
{
public:
	IDrawable();
	virtual ~IDrawable();

	virtual void draw(sf::RenderWindow* _window) { _window->draw(sprite); }

	// set's texture of object's sprite
	void setTexture(sf::Texture* _texture);
	// returns pointer to object's sprite
	const sf::Sprite* getSprite() { return &sprite; }

	// Get position
	const sf::Vector2f& GetPosition() { return sprite.getPosition(); };

protected:
	sf::Sprite sprite;
};
#endif // !_DRAWABLE_H_