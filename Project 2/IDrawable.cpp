#include "IDrawable.h"
#include <math.h>

IDrawable::IDrawable()
{
	sprite = sf::Sprite();
	// TODO Check if this works as intended, replace later with default texture from assetManager
	sf::Texture* texture = new sf::Texture();
	texture->loadFromFile("./Assets/default.png");
	sprite.setTexture(*texture);
	sprite.setOrigin(sf::Vector2f(texture->getSize().x * 0.5, texture->getSize().y * 0.5)); // Important
	//sprite.setPosition(window->getSize().x * 0.5f, window->getSize().y * 0.5f) // Don't forget to uncomment and finish this line
}

IDrawable::~IDrawable()
{

}

void IDrawable::setTexture(sf::Texture* _texture)
{
	const sf::Texture* oldTexture = sprite.getTexture();

	if(oldTexture != nullptr) 
		delete oldTexture;

	sprite.setTexture(*_texture);
	sprite.setOrigin(sf::Vector2f(_texture->getSize().x * 0.5, _texture->getSize().y * 0.5));
}