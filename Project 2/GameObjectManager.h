#pragma once
#ifndef _OBJECT_MANAGER_H_
#define _OBJECT_MANAGER_H_

class GameObject;
#include <list>
#include "ILoadable.h"
#include "StandardTypes.h"

class GameObjectManager final : public ILoadable
{
	DECLARE_SINGLETON(GameObjectManager)
public:
	std::list<GameObject*> objects;
	std::list<GameObject*> objects_to_remove;

	void load(json::JSON& _jsonObject);

	GameObject* CreateGameObject();
	void AddGameObject(GameObject* _gameObject);
	void RemoveGameObject(GameObject* _gameObject);
};
#endif // !_OBJECT_MANAGER_H_