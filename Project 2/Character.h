#pragma once
#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include "GameObject.h"

class Character : public GameObject
{
public:
	Character();
	virtual ~Character();

	virtual void update() override;
	virtual void load(json::JSON& _json) override;
	virtual void Die();
	virtual void Turn();

private:
	int maxHealth;
	int health;
	bool dead;
};
#endif // !_CHARACTER_H_