#pragma once
#ifndef _OBJECT_H_
#define _OBJECT_H_

#include "IInitializable.h"
#include "ILoadable.h"
#include "IDrawable.h"

class Object : public ILoadable, public IDrawable
{
public:
	// Constructors / Destructors
	Object();
	virtual ~Object() {};

	virtual void update() = 0;
	virtual void load(json::JSON& _json) override;

	void SetPosition(const sf::Vector2i& _pos);
	void SetPosition(unsigned int x, unsigned int y);

	void Move(const sf::Vector2i& _pos);
	void Move(unsigned int x, unsigned int y);

	bool IsColliding(Object* _target);
	virtual void OnCollision(Object* _target);

	sf::Vector2i GetPosition() { return position; }

	bool blockMovement;

protected:
	sf::Vector2i position;
	std::string texturePath;
};
#endif // !_OBJECT_H_

