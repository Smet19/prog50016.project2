#pragma once
#ifndef _SETTINGS_MANAGER_H_
#define _SETTINGS_MANAGER_H_

#include "IInitializable.h"
#include "StandardTypes.h"
#include "json.hpp"

class SettingsManager :  public IInitializable
{
public:
	SettingsManager();
	virtual ~SettingsManager();

	void initialize() override;

	json::JSON& getSettings() { return settings; }
	json::JSON& getMap() { return map; }
	json::JSON& getGameObjects() { return game_objects; }

	std::string WindowName() { return windowName; }
	int Width() { return width; }
	int Height() { return height; }
	int FrameLimit() { return frameLimit; }
	bool Fullscreen() { return fullscreen; }
	std::string UIFontPath() { return uiFontPath; }

private:
	json::JSON settings;

	std::string windowName;
	int width;
	int height;
	int frameLimit;
	bool fullscreen;
	std::string uiFontPath;

	json::JSON map;
	json::JSON game_objects;

	DECLARE_SINGLETON(SettingsManager)
};

#endif // !_SETTINGS_MANAGER_H_

