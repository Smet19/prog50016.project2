#pragma once
#ifndef _GAME_OBJECT_H_
#define _GAME_OBJECT_H_

#include "Object.h"

class GameObject : public Object
{
private:
    friend class GameObjectManager;

public:
    GameObject();
    virtual ~GameObject();
    virtual void update() override;
    virtual void load(json::JSON& _json) override;
};
#endif // !_GAME_OBJECT_H_