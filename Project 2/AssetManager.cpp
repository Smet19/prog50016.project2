#include "AssetManager.h"

AssetManager::AssetManager()
{
}

AssetManager::~AssetManager()
{
	// Deleting every texture from map
	for (auto const& entry : textureMap)
	{
		delete entry.second;
	}
}

void AssetManager::initialize()
{
	if (!initialized)
	{
		// Pre-load default texture
		sf::Texture* defaultTexture = new sf::Texture();
		if (defaultTexture->loadFromFile("./Resources/default.png"))
		{
			textureMap.emplace("./Resources/default.png", defaultTexture);
		}
		initialized = true;
	}
}

// Gets preloaded texture. If texture isn't loaded - will attempt to load one
sf::Texture* AssetManager::getTexture(std::string _fileName)
{
	if (!textureMap.empty() && textureMap.find(_fileName) != textureMap.end())
	{
		return textureMap[_fileName];
	}
	sf::Texture* texture = new sf::Texture();

	if (!texture->loadFromFile(_fileName))
	{
		throw std::invalid_argument("Unnable to load texture file from " + _fileName + " . Stopping program");
	}

	textureMap.emplace(_fileName, texture);
	return texture;
}