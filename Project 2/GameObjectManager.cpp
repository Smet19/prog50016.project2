#include "GameObjectManager.h"
#include "GameObject.h"

void GameObjectManager::load(json::JSON& _jsonObject)
{
    if (_jsonObject.hasKey("game_objects"))
    {
        json::JSON gameObjects = _jsonObject["game_objects"];
        {
            for (auto gameObjectNode : gameObjects.ArrayRange())
            {
                GameObject* gameObject = new GameObject();
                gameObject->load(gameObjectNode);
                AddGameObject(gameObject);
            }
        }
    }
}

GameObject* GameObjectManager::CreateGameObject()
{
	return nullptr;
}

void GameObjectManager::AddGameObject(GameObject* _gameObject)
{
    objects.push_back(_gameObject);
}

void GameObjectManager::RemoveGameObject(GameObject* _gameObject)
{
    objects_to_remove.push_back(_gameObject);
}

GameObject* GameObjectManager::CreateGameObject()
{
    GameObject* go = new GameObject();
    AddGameObject(go);
    return go;
}